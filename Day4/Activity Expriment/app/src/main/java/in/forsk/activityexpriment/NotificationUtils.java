package in.forsk.activityexpriment;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;

/**
 * Created by Saurabh on 3/5/2016.
 */
public class NotificationUtils {

    @SuppressLint("NewApi")
    public static void notify(Context context,String methodName) {
        String name = context.getClass().getName();
        String[] strings = name.split("\\.");
        Notification noti = new
                Notification.Builder(context).setContentTitle(methodName + " " +
                strings[strings.length -
                        1]).setAutoCancel(true).setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(name).build();
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify((int) System.currentTimeMillis(), noti);
    }
}
